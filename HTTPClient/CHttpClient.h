#pragma once
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>

using namespace std;

#pragma comment(lib, "ws2_32.lib")
class CHttpClient
{
public:
	CHttpClient();
	~CHttpClient();


	void socketHttp(std::string host, std::string request);

	void postRequest(std::string host, std::string path, std::string post_content);
	void getRequest(std::string host, std::string path, std::string get_content);


private:
	unsigned short _port = 802; //�˿�
	char _revcbuf[1024 * 3];

};

